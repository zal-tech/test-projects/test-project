# node-test-project

Test project for experimenting with npm/gitlab registries, cicd/automation of releases, etc.

### Tools / Libraries Used:

-   chai (Test expectations)
-   config (Application config)
-   eslint (Code quality)
-   [eslint-conventional-changelog](https://github.com/conventional-changelog/conventional-changelog/tree/master/packages/conventional-changelog-eslint) (Commit / changelog formatting)
-   mocha (Test suite)
-   prettier (Code formatting)
-   semantic-release (Automate version bumping, tagging, and publishing)
-   sinon (Test mocking)
-   ts-node (In-memory transpiler for local execution)
-   webpack (Code bundling)
-   winston (Logging)
