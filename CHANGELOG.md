# [0.8.0](https://gitlab.com/zal-tech/test-project/compare/v0.7.0...v0.8.0) (2021-11-14)


### New

* Added Mocha/NYC reporting to CI ([c5e7175](https://gitlab.com/zal-tech/test-project/commit/c5e7175192f9af0425485ff66412251a95ce90e2))

# [0.7.0](https://gitlab.com/zal-tech/test-project/compare/v0.6.3...v0.7.0) (2021-04-21)


### Fix

* Fixed the aws-sam-webpack url. ([195fc20](https://gitlab.com/zal-tech/test-project/commit/195fc20c5ccd51f77cd3b01326e4f05c5a851333))

### New

* Added support for code coverage, and changed formatting. ([672b1f5](https://gitlab.com/zal-tech/test-project/commit/672b1f5f94784f0d95100e052f05f7c0e76a8a9c))

## [0.6.3](https://gitlab.com/zal-tech/test-project/compare/v0.6.2...v0.6.3) (2021-04-06)


### Fix

* Testing another fix for publishing. ([c16772c](https://gitlab.com/zal-tech/test-project/commit/c16772c290c8a655e34a68d6eb92e3559ae9ee99))

## [0.6.2](https://gitlab.com/zal-tech/node-test-project/compare/v0.6.1...v0.6.2) (2021-04-06)


### Fix

* Testing another fix for publishing. ([d6d359e](https://gitlab.com/zal-tech/node-test-project/commit/d6d359e20473ed17efb378cd7dd2ea7b6c755a2a))

## [0.6.1](https://gitlab.com/zal-tech/node-test-project/compare/v0.6.0...v0.6.1) (2021-04-06)


### Fix

* Fixed release config to properly create a package. ([91ff230](https://gitlab.com/zal-tech/node-test-project/commit/91ff2309b3b89a031604b0c638566f5d202b3c28))
* Fixed release config to properly create a package. ([07e3a40](https://gitlab.com/zal-tech/node-test-project/commit/07e3a4040f28a2cf2e4bfa8cba2d7421b4df3f8c))

# [0.6.0](https://gitlab.com/zal-tech/node-test-project/compare/v0.5.0...v0.6.0) (2021-04-01)

### Update

-   Triggering a new release for test purposes. ([533e1e9](https://gitlab.com/zal-tech/node-test-project/commit/533e1e990855a8548546f3a4ae0d858e58cdb396))

# [0.5.0](https://gitlab.com/zal-tech/node-test-project/compare/v0.4.0...v0.5.0) (2021-04-01)

### Update

-   Triggering a new release for test purposes. ([6ddeaba](https://gitlab.com/zal-tech/node-test-project/commit/6ddeabac379c93bcd18ab093916cb507610ae1af))
