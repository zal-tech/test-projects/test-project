"use strict";

module.exports = {
    require: ["ts-node/register", "tsconfig-paths/register"],
    spec: "src/**/*.unit.ts",
    timeout: 2500,
    ui: "bdd",
};
